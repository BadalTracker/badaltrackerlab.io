import el from './elements.js';
import { getData } from './app.js';
import { Dexie } from 'https://unpkg.com/dexie/dist/modern/dexie.min.mjs';

const db = new Dexie('ImageDatabase');
db.version(1).stores({
    images: '++id, filename, data'
});

let canvas = document.createElement('canvas');
let ctx = canvas.getContext('2d');

async function storeImage(file) {
    const bitmap = await createImageBitmap(file);
    try {
        const aspectRatio = bitmap.width / bitmap.height;
        const newHeight = 480;
        const newWidth = newHeight * aspectRatio;

        [canvas.width, canvas.height] = [newWidth, newHeight];
        ctx.drawImage(bitmap, 0, 0, newWidth, newHeight);

        const date = new Date();
        const newFilename = `image${date.toISOString().replace(/:/g, '').replace(/\..+/, '')}.webp`;

        const blob = await new Promise(resolve => {
            canvas.toBlob(resolve, 'image/webp');
        });

        const result = await db.images.add({ filename: newFilename, data: blob });
        console.table([
            {
              "File Name": newFilename,
              "File ID": result,
              "File Size": `${(blob.size / 1024).toFixed(2)} KB`
            }
          ]);
        return result;
    } finally {
        bitmap.close();
    }
}

async function setImageDetail(id) {
    try {
        el.bookDetailImage.src = id
            ? URL.createObjectURL((await db.images.get(id))?.data)
            : 'images/r.webp';
    } catch (error) {
        console.error('Error retrieving image:', error);
        el.bookDetailImage.src = 'images/r.webp';
    }
}

async function deleteUnusedImages() {
    const year = el.yearSelect.value;
    const Data = getData();
    const imageIds = Data[year]?.map((entry) => parseInt(entry.image))
        .filter((id) => id !== null && !isNaN(id));

    if (imageIds?.length > 0) {
        try {
            const allImages = await db.images.toArray();
            const allImageIds = allImages.map((image) => image.id);

            const idsToDelete = allImageIds.filter((id) => !imageIds.includes(id));

            await Promise.all(idsToDelete.map((id) => db.images.delete(id)));

            console.log('Unused images cleared from Database ✅');
        } catch (error) {
            console.error('Error deleting images:', error);
        }
    }
}

deleteUnusedImages();

export { storeImage, setImageDetail };