import el from './elements.js';
import { storeImage, setImageDetail } from './database.js';

console.log("%c       ﴾ بسم الله ﴿", "font-family: 'Arabic Typesetting', serif; font-size: 2em; line-height: 3");

let year = el.yearSelect.value;

function getData() {
    return JSON.parse(localStorage.getItem('yearlyEntries')) ?? {};
}

// Add Budget
el.addBudget.addEventListener('click', () => {
    const currentBudget = localStorage.getItem('additionalBudget');
    let userInput = prompt(`Current additional budget: ${currentBudget ? currentBudget : 'None'}\n\nPlease enter the new additional budget:`);
    if (userInput === null) return;

    userInput = userInput.trim();
    let parsedInput = parseFloat(userInput);

    if (!isNaN(parsedInput) && userInput !== '') {
        localStorage.setItem('additionalBudget', userInput);
    } else {
        alert('Invalid');
    }
});

// snack
function setMessage(message) {
    el.success.setAttribute('message', message);
    el.success.show();
}

// Budget Handling
function updateBudget() {
    const base = 850;
    const Data = getData();
    const totalPrice = Data[year]?.reduce((acc, { price }) => {
        return acc + (price ? +price : 0);
    }, 0) ?? 0;

    const budgetNumberValue = base - totalPrice;
    el.budgetNumber.textContent = budgetNumberValue;

    const additionalBudgetValue = +localStorage.getItem('additionalBudget') ?? 0;
    const isDeficit = budgetNumberValue < 0;

    el.additionalBudgetEl.textContent = `+${additionalBudgetValue}`;

    el.additionalBudgetEl.style.display = additionalBudgetValue ? '' : 'none';

    el.budgetBar.value = (budgetNumberValue + +additionalBudgetValue) / (base + +additionalBudgetValue);

    el.budgetBar.style.cssText = isDeficit
        ? `--md-linear-progress-active-indicator-color: var(--md-sys-color-error)`
        : el.budgetBar.value <= 0.5
            ? `--md-linear-progress-active-indicator-color: #F18F01`
            : '';
}

// List Handling
function updateList() {
    const Data = getData();

    el.bookList.innerHTML = '';

    if (Data && Data[year] && Array.isArray(Data[year])) {
        Data[year].forEach((book, index) => {
            const listItem = document.createElement('md-list-item');
            listItem.type = 'button';

            const listItemContent = `
                <md-icon class="book-icon" slot="start" filled>${book.copy ? 'contract' : 'book_5'}</md-icon>
                <div class="item-book" slot="headline">${book.book || ""}</div>
                <div class="item-store" slot="supporting-text">${book.store ?? ""}</div>
                <div class="item-date" slot="trailing-supporting-text">${book.date || "<span style=\"font-family: 'Amiri';\">إن شاء الله</span>"}</div>
                <div class="item-price" slot="trailing-supporting-text">${book.price || 0}</div>
                <md-icon class="end-icon ${book.wishlist ? 'green' : ''}" slot="end" filled>${book.wishlist ? 'favorite' : 'check_circle'}</md-icon>
            `;

            listItem.innerHTML = listItemContent;
            el.bookList.appendChild(listItem);

            listItem.addEventListener('click', () => {
                openDetail(index);
            });
        });
    }
}

// Book Detail
function openDetail(index) {
    const Data = getData();
    const book = Data[year][index];

    setImageDetail(parseInt(book.image, 10));

    Object.keys(book).forEach((key) => {
        const field = el.bookDetailDialog.querySelector(`#book-detail-${key}`);
        if (field) {
            field.textContent = book[key];
        }
    });

    el.detailIcon.textContent = book.wishlist ? 'favorite' : 'book';
    el.detailIcon.style.color = book.wishlist ? '#90BE6D' : '';
    el.detailMeta.style.display = book.wishlist ? 'none' : 'flex';
    el.bookDetailWishlist.style.display = book.wishlist ? 'flex' : 'none';

    currentIndex = index;
    el.bookDetailDialog.show();
}

let currentIndex;

el.deleteButton.addEventListener('click', (e) => {
    e.preventDefault();
    const Data = getData();

    Data[year] = Data[year].filter((_, i) => i !== currentIndex);
    localStorage.setItem('yearlyEntries', JSON.stringify(Data));
    el.bookDetailDialog.close();
    setMessage('Book deleted');
});


// New Book Handling
fab.addEventListener('click', () => {
    currentIndex = -1;
    isEditMode = false;
    el.addBookDialog.open = true;
    el.chipWishlist.selected = false;
});

el.saveButton.addEventListener('click', async (event) => {
    event.preventDefault();
    const formData = await getFormData();
    (isEditMode ? saveEdit : saveData)(formData);
    el.addBookDialog.close('save');
    el.addBookForm.reset();
});

async function getFormData() {
    const formData = {};
    const inputs = [...document.querySelectorAll('[id^="input-"]')];
    if (el.inputProxyImage && el.inputProxyImage.files.length > 0) {
        const proxyImageFile = Array.from(el.inputProxyImage.files)[0];
        const imageId = await storeImage(proxyImageFile);
        if (el.imageInput) {
            el.imageInput.value = imageId;
        }
    }

    const wishlistInput = document.getElementById('input-wishlist');

    for (const el of inputs) {
        const id = el.id.replace('input-', '');
        const value = id === 'date' && !wishlistInput.value ? new Date().toISOString().slice(0, 10) : (id === 'date' ? '' : el.value);
        formData[id] = value;
    }

    return formData;
}

function saveData(formData) {
    const Data = getData();
    Data[year] = (Data[year] ?? []).concat([formData]);
    localStorage.setItem('yearlyEntries', JSON.stringify(Data));
    setMessage('Book added');
}

// Edit Handling
let isEditMode = false;

el.editButton.addEventListener('click', (e) => {
    e.preventDefault();
    editBook(currentIndex);
});

function editBook(index) {
    const Data = getData();
    const book = Data[year][index];

    // Populate input fields with book data
    Object.keys(book).forEach((key) => {
        const field = document.querySelector(`#input-${key}`);
        if (field && field.type !== 'file') {
            field.value = book[key];
        }
    });

    if (el.inputWishlist && el.inputWishlist.value) {
        el.chipWishlist.selected = true;
    } else {
        el.chipWishlist.selected = false;
    }

    isEditMode = true;
    currentIndex = index;

    el.addBookDialog.open = true;
}

function saveEdit(formData) {
    const Data = getData();
    Data[year][currentIndex] = formData;
    localStorage.setItem('yearlyEntries', JSON.stringify(Data));

    openDetail(currentIndex);
    setMessage('Book edited');
}

// params
const urlParams = new URLSearchParams(window.location.search);
const bookTitle = urlParams.get('book');

window.addEventListener('load', () => {
    if (bookTitle) {
        el.addBookDialog.show();
        document.getElementById('input-book').value = bookTitle;
    }
});

// about
el.mainIcon.addEventListener('click', () => {
    el.about.show();
});

// state update
document.addEventListener("localStorageUpdated", (e) => {
    const value = JSON.parse(e.value);
    console.log(value)
    setTimeout(updateUI, 0);
});

document.addEventListener('yearSelectChanged', (e) => {
    console.log(`Year: ${e.detail.newValue}`);
    year = e.detail.newValue;
    updateUI();
});

function updateUI() {
    updateBudget();
    updateList();
}

updateUI();

export { getData };