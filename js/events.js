import el from './elements.js';

el.menuButton.addEventListener('click', () => { el.menuList.open = !el.menuList.open; });

const innerFab = document.querySelector('md-fab').shadowRoot.querySelector('.fab');
innerFab.style.transition = "width 0.2s cubic-bezier(0.3, 0, 0.8, 0.15)";

window.addEventListener('scroll', () => {
  const scrolled = window.scrollY > 0;
  el.header.classList.toggle('scrolled', scrolled);
  el.fab.label = scrolled ? '' : 'ADD';
  innerFab.classList.toggle('extended', !scrolled);
  innerFab.style.width = scrolled ? '56px' : '98px';
});

el.imageButton.addEventListener('click', () => {
  el.inputProxyImage.click();
});

el.dateButton.addEventListener('click', () => {
  el.dateInput.showPicker();
});

if (!(typeof el.dateInput.showPicker === 'function')) {
  // iOS 👎
  console.log('iOS 👎');
  el.dateButton.style.display = 'none';
  el.dateInput.style.display = 'flex';
}

const mdIcon = document.querySelector('#image-button md-icon');

el.inputProxyImage.addEventListener('change', (e) => {
  const file = el.inputProxyImage.files[0];
  if (file) {
    console.log('File:', file);
    mdIcon.textContent = 'check';
  }
});

el.addBookDialog.addEventListener('close', () => {
  mdIcon.textContent = 'add_a_photo';
});

el.bookDetailImage.addEventListener('click', () => {
  el.bookDetailImage.classList.toggle('expanded');
});

window.addEventListener('load', () => {
  el.loading.style.opacity = '0';
  el.loading.style.height = '35%';
  setTimeout(() => {
    el.loading.style.display = 'none';
  }, 600);

});

el.refresh.addEventListener('click', () => window.location.reload(true));

//observer

function observeSelectedAttribute(element, callback) {
  const observer = new MutationObserver(() => {
    const isSelected = element.hasAttribute('selected');
    callback(isSelected);
  });

  const options = {
    attributeFilter: ['selected'],
    attributeOldValue: true,
  };

  observer.observe(element, options);
}

observeSelectedAttribute(el.chipWishlist, (isSelected) => {
  el.inputWishlist.value = isSelected ? 'true' : '';
  el.dateButton.disabled = isSelected ? true : false;
  
});

observeSelectedAttribute(el.chipCopy, (isSelected) => {
  el.inputCopy.value = isSelected ? 'true' : '';
  el.publisherVolumes.style.display = isSelected ? 'none' : '';
});